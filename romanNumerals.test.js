/Testing romanNumerals.js

const romanNumerals = require('./romanNumerals')

describe("Testing conversion of Arabic numbers to Roman numerals", () => {

    it('initial test', () => {
        romanNumerals()
    })

    it('Returns I when number is 1', () => {
        expect(romanNumerals(1)).toBe("I")
    })

    it('Returns II when number is 2', () => {
        expect(romanNumerals(2)).toBe("II")
    })

    it('Returns IV when number is 4', () => {
        expect(romanNumerals(4)).toBe("IV")
    })

    it('Returns V when number is 5', () => {
        expect(romanNumerals(5)).toBe("V")
    })

    it('Returns VI when number is 6', () => {
        expect(romanNumerals(6)).toBe("VI")
    })

    it('Returns VIII when number is 8', () => {
        expect(romanNumerals(8)).toBe("VIII")
    })

    it('Returns IX when number is 9', () => {
        expect(romanNumerals(9)).toBe("IX")
    })

    it('Returns X when number is 10', () => {
        expect(romanNumerals(10)).toBe("X")
    })

    it('Returns XI when number is 11', () => {
        expect(arabicToRoman(11)).toBe("XI")
    })

    it('Returns XIII when number is 13', () => {
        expect(arabicToRoman(13)).toBe("XIII")
    }) 

    it('Returns XV when number is 15', () => {
        expect(arabicToRoman(15)).toBe("XV")
    })

    it('Returns XL when number is 40', () => {
        expect(arabicToRoman(40)).toBe("XL")
    })

    it('Returns L when number is 50', () => {
        expect(arabicToRoman(50)).toBe("L")
    })

    it('Returns XC when number is 90', () => {
        expect(arabicToRoman(90)).toBe("XC")
    })

    it('Returns C when number is 100', () => {
        expect(arabicToRoman(100)).toBe("C")
    })

    it('Returns CD when number is 400', () => {
        expect(arabicToRoman(400)).toBe("CD")
    })

    it('Returns D when number is 500', () => {
        expect(arabicToRoman(500)).toBe("D")
    })

    it('Returns CM when number is 900', () => {
        expect(arabicToRoman(900)).toBe("CM")
    })
    
    it('Returns M when number is 1000', () => {
        expect(arabicToRoman(1000)).toBe("M")
    })
})